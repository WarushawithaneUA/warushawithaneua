﻿namespace CodedUITestProjectWarushawithane
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;


    public partial class UIMap
    {
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }
        /// <summary>
        /// SimpleAppTestWarushawithane - Use 'SimpleAppTestWarushawithaneParams' to pass parameters into this method.
        /// </summary>
        public void ModifiedSimpleAppTestWarushawithane()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            WinButton uICloseButton = this.UIMainWindowWindow1.UICloseButton;
            #endregion

            // Launch 'F:\Project\SimpleWPFAppWarushawithane\SimpleWPFAppWarushawithane\bin\Debug\SimpleWPFAppWarushawithane.exe'
            ApplicationUnderTest uIMainWindowWindow = ApplicationUnderTest.Launch(this.SimpleAppTestWarushawithaneParams.UIMainWindowWindowExePath, this.SimpleAppTestWarushawithaneParams.UIMainWindowWindowAlternateExePath);

            // Click 'Start' button
            Mouse.Click(uIStartButton, new Point(48, 18));
            uICheckBoxCheckBox.WaitForControlEnabled();
            // Select 'CheckBox' check box
            uICheckBoxCheckBox.Checked = this.SimpleAppTestWarushawithaneParams.UICheckBoxCheckBoxChecked;

            // Click 'Close' button
            Mouse.Click(uICloseButton, new Point(22, 16));
        }

        public virtual SimpleAppTestWarushawithaneParams SimpleAppTestWarushawithaneParams
        {
            get
            {
                if ((this.mSimpleAppTestWarushawithaneParams == null))
                {
                    this.mSimpleAppTestWarushawithaneParams = new SimpleAppTestWarushawithaneParams();
                }
                return this.mSimpleAppTestWarushawithaneParams;
            }
        }

        private SimpleAppTestWarushawithaneParams mSimpleAppTestWarushawithaneParams;
    }
    /// <summary>
    /// Parameters to be passed into 'SimpleAppTestWarushawithane'
    /// </summary>
    [GeneratedCode("Coded UITest Builder", "16.0.32802.440")]
    public class SimpleAppTestWarushawithaneParams
    {

        #region Fields
        /// <summary>
        /// Launch 'F:\Project\SimpleWPFAppWarushawithane\SimpleWPFAppWarushawithane\bin\Debug\SimpleWPFAppWarushawithane.exe'
        /// </summary>
        public string UIMainWindowWindowExePath = "F:\\Project\\SimpleWPFAppWarushawithane\\SimpleWPFAppWarushawithane\\bin\\Debug\\Simple" +
            "WPFAppWarushawithane.exe";

        /// <summary>
        /// Launch 'F:\Project\SimpleWPFAppWarushawithane\SimpleWPFAppWarushawithane\bin\Debug\SimpleWPFAppWarushawithane.exe'
        /// </summary>
        public string UIMainWindowWindowAlternateExePath = "F:\\Project\\SimpleWPFAppWarushawithane\\SimpleWPFAppWarushawithane\\bin\\Debug\\Simple" +
            "WPFAppWarushawithane.exe";

        /// <summary>
        /// Select 'CheckBox' check box
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = true;
        #endregion
    }
}
